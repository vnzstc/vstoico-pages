const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

app.engine('.html', require('ejs').__express)
app.set('view engine', 'html');

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'public/views'))

app.get('/', function(req, res){
  res.render('page', { htmlContent: 'index.html', activePage: 'index' });
});

app.get('/publications', function(req, res){
  res.render('page', { htmlContent: 'pub.html', activePage: 'pub' });
});

app.get('/research', function(req, res){
  res.render('page', { htmlContent: 'research.html', activePage: 'research' });
});

// Start the server
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
