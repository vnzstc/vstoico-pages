let publications = [];
var searchInput = document.getElementById('search-input');
var searchResults = document.getElementById('search-results');

const { index, data } = await setupLunr();
displayAllDocuments()

async function setupLunr() {
  try {
    const response = await fetch('../js/publications.json');
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    
    const idx = lunr(function() {
      this.ref('id');
      this.field('authors');
      this.field('title');     
      this.field('venue');
      this.field('type');
      this.field('year');

      //publications = data;
      publications = structuredClone(data);
      data.forEach(function(doc) {
        doc.authors = doc.authors.join(" ");
        this.add(doc);
      }, this);
    });
    
    return { index: idx, data: data };

  } catch (error) {
    console.error('Error setting up Lunr:', error);
  }
}

function displayAllDocuments() {
  searchResults.innerHTML = '';
  publications.sort((a, b) => b.year - a.year);
  publications.forEach(entry => {
    const newCard = createCard(
      entry.id, entry.title, entry.authors, entry.venue, entry.type, entry.year
    );
    searchResults.insertAdjacentHTML('beforeend', newCard);
  });
  attachBibtexListener();
}

searchInput.addEventListener('input', function() {
  var query = this.value.toLowerCase();

  if(query.length < 2) {
    displayAllDocuments();
    return;
  }

  var searchTerms = query.split(' ').filter(el => el !== '');
  // exact match search
  var results = index.search(query);
  // if no results, perform wildcard search
  if (results.length === 0) {
    results = index.search(query + '*');
  }

  displayResults(results, searchTerms);
});

function displayResults(results, searchTerms) {
  searchResults.innerHTML = '';

  if(results.length === 0) {
    searchResults.innerHTML = '<p>No results found</p>';
    return;
  }
  
  var resultSet = [] 
  results.forEach(function(result) {
    const { ref, score, matchData } = result;
    const resultData = publications.find(item => item.id === ref);
    resultSet.push(resultData);
  });

  resultSet.sort((a, b) => b.year - a.year);
  resultSet.forEach(function(result) {
    const newCard = createCard(
      result.id, result.title, result.authors, result.venue, result.type, result.year
    );
    searchResults.insertAdjacentHTML('beforeend', newCard);
  });

  const instance = new Mark(document.getElementById('content'))
  instance.mark(searchTerms, {
    separateWordSearch: false
  });

  attachBibtexListener();
}

function createCard(id, title, authors, venue, type, year) {
  let displayedAuthors = structuredClone(authors);
  var authorIdx = displayedAuthors.indexOf('Vincenzo Stoico');
  if(authorIdx != -1) {
    displayedAuthors[authorIdx] = `<b>Vincenzo Stoico</b>`;
  }

  const cardHTML = `
    <div id="${id}" class="card mb-4 p-4">
      <div class="card-body">
        <h4 class="card-title">${title}</h4>
        <p id="year" class="d-inline-block card-text mb-2">${type} - ${year}</p>
        <p id="venue" class="card-text mb-2">${venue}</p>
        <p id="authors" class="card-text mb-2">${displayedAuthors}</p>
      </div>
      <div class="d-flex justify-content-end align-items-center">
        <a href="../papers/${id}.pdf" role="button" class="card-end me-2">
          .pdf
        </a>
        <a role="button" class="gen-bibtex card-end me-2">
          .bibtex
        </a>
      </div>
    </div>
  `;
  return cardHTML;
}

function attachBibtexListener() {
  document.querySelectorAll('.gen-bibtex').forEach(item => {
    item.addEventListener('click', event => {
      var card_id = item.closest('.card').id;
      var paper = publications.find(obj => obj.id === card_id);
      var newWindow = window.open();

      newWindow.document.write('<pre>' + jsonToBibtex(paper) + '</pre>');
      newWindow.document.close();
    });
  });
}
// Code Generated with Perplexity 
function jsonToBibtex(entry) {
  let bibtexType = '';
  let venueField = '';

  // Determine the type of entry and venue field
  if (entry.type === 'Journal') {
    bibtexType = 'article';
    venueField = `journal = {${entry.venue}},`;
  } else if (['Conference', 'Workshop', 'Poster'].includes(entry.type)) {
    bibtexType = 'inproceedings';
    venueField = `booktitle = {${entry.venue}},`;
  } else {
    bibtexType = 'misc'; // Default type if venue_type is not specified
  }

  // Construct the BibTeX entry
  let bibtexEntry = `@${bibtexType}{${entry.id},\n`;
  bibtexEntry += `  title = {${entry.title}},\n`;
  bibtexEntry += `  author = {${transformNames(entry.authors)}},\n`;
  bibtexEntry += `  year = {${entry.year}},\n`;

  if (venueField) {
    bibtexEntry += `  ${venueField}\n`;
  }

  // Add optional fields if they exist
  if (entry.volume) bibtexEntry += `  volume = {${entry.volume}},\n`;
  if (entry.number) bibtexEntry += `  number = {${entry.number}},\n`;
  if (entry.pages) bibtexEntry += `  pages = {${entry.pages}},\n`;
  if (entry.doi) bibtexEntry += `  doi = {${entry.doi}},\n`;
  if (entry.url) bibtexEntry += `  url = {${entry.url}},\n`;

  // Remove the last comma and close the entry
  bibtexEntry = bibtexEntry.replace(/,\n$/, '\n');
  bibtexEntry += `}\n\n`;

  return bibtexEntry;
}

function transformNames(namesArray) {
  return namesArray.map(fullName => {
    const [name, ...surname] = fullName.split(' ');
    return `${surname.join(' ')}, ${name}`;
  }).join(' and ');
}


function performSearch(idx, query) {
  // exact match search
  let results = idx.search(query);

  // If no results, perform wildcard search
  if (results.length === 0) {
    results = idx.search(query + '*');
  }

  return results;
}
