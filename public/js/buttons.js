let mybutton = document.getElementById("btn-back-to-top");

window.onscroll = function() {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

mybutton.addEventListener("click", backToTop);
function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

// dark-theme toggle
const toggle = document.getElementById('theme-toggle');
const toggleIcon = toggle.getElementsByTagName('i')[0];

toggle.addEventListener('click', () => {
  const html = document.documentElement;
  if (html.getAttribute('data-bs-theme') === 'dark') {
    html.setAttribute('data-bs-theme', 'light');

    toggleIcon.classList.remove('bi-lightbulb');
    toggleIcon.classList.add('bi-lightbulb-fill');

  } else {
    html.setAttribute('data-bs-theme', 'dark');

    toggleIcon.classList.remove('bi-lightbulb-fill');
    toggleIcon.classList.add('bi-lightbulb');
  }
});
